package com.mindtree.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.mindtree.hello.MavenGitIntegeration;

public class DemoTest {
	MavenGitIntegeration integration = new MavenGitIntegeration();

	@Test
	public void testgetGoodMorning() {
		assertEquals("Good Morning", integration.getGoodMorning());
	}

	@Test
	public void testgetGoodNight() {
		assertEquals("Good Night", integration.getGoodNigt());
	}
}
